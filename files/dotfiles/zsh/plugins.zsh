# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# My zsh plugins
plugins=(
  git
  ansible
  colored-man-pages
  colorize
  common-aliases
  rsync
  systemd
  ubuntu
  zsh-syntax-highlighting
  zsh-autosuggestions
)
