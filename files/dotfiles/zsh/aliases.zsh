# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

alias mtr='mtr -t'
alias today='find . -maxdepth 1 -newermt "$(date +%Y-%m-%d)"'